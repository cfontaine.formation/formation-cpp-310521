#ifndef WINDOW_H
#define WINDOW_H

#include "Bouton.h"

namespace programme
{
    namespace gui
    {
        enum class Etat
        {
            WINDOWED,
            FULLSCREEN
        };
        class Window
        {
            int valeur;
            Bouton bp;

        public:
            Window(int valeur) : valeur(valeur) {}

            void show(){};
        };
    }

}

#endif