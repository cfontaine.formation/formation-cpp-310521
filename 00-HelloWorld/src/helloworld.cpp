#include <iostream>     // inclure le fichier d'en-t�te iostream qui d�finit les objets de flux d'entr�e/sortie standard
using namespace std;    // placer tous les �l�ments de l'espace de nom std dans la port�e

/* Commentaire sur 
    plusieurs 
    lignes */

// Commentaire fin de ligne

// Fonction main => point d'entr� du programme
int main()
{ 
    cout<<"Hello World!"<<endl;     // cout => �crire dans le flux de sortie (console),  endl => retour � la ligne
    return 0;                       // Si le code, c'est ex�cut� normalement, la fonction main doit retourner 0 
}