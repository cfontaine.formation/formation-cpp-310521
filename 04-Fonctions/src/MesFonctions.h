// Fichier .h => contient la déclaration des fonctions

#ifndef MES_FONCTIONS_H // Indique au compilateur de n'intègrer le fichier d’en-tête qu’une seule fois, lors de la compilation d’un fichier de code source
#define MES_FONCTIONS_H // ou # pragma once

#include <initializer_list>

// déclaration d'une variable qui a été définie dans main.cpp. Il n' y a pas d'allocation mémoire
extern const int testVarExtern;
//extern double pi; // pi est static dans main.cpp, on ne peut l'utiliser que dans ce fichier

// Déclaration d'une fonction
int somme(int a, int b);

// Déclaration d'une fonction sans retour => void
void afficher(int a);

// Passage de paramètre par valeur
void testParamValeur(int);

// On peut définir des valeurs par défaut pour les paramètres. Ils doivent se trouver en fin de liste des arguments
// On place les valeurs par défaut dans la déclaration des fonctions
void testParamDefault(double d, int i = 34, char c = 'a');

// Exercice maximum
double maximum(double, double);

// Exercice parité
bool even(int val);

// Passage de paramètre par adresse
// En utilisant le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre
void testParamAdresse(int *);

// Passage d'un tableau, comme paramètre d'une fonction
// On ne peut pas passer un tableau par valeur uniquement par adresse
void afficherTableau(int *, int);
void afficherTableau2(int[], int); // pour le passage de tablea en paramètre on peut aussi utiliser cette syntaxe

// Passage de paramètre par référence
// Comme avec  le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre
void testParamReference(int &);

void testParamReferenceCst(const int &a = 123);

// Fonction à nombre variable de paramètre  (hérité du C)
double moyenne(int nbArgs, ...);

// Fonction a nombre variable de paramètre  (C++11)
double moyenneC11(std::initializer_list<int> varg);

// Déclaration d'un fonction inline, on place le code de la fonction dans le fichier.h
// inline => permet de définir des fonctions qui seront directement évaluées à la compilation
inline int doubler(int v) // inline
{
    return 2 * v;
}

// Surcharge de la fonction multiplication
int multiplier(int a, int b);
double multiplier(double a, double b);
double multiplier(int a, double b);
int multiplier(int a, int b, int c);

// Récursivité
int factorial(int n);

// ERREUR => fonctions qui retournent une référence ou un pointeur sur une variable locale
int* testRetour();

// Exercices méthodes: tableau
int* saisirTableau(int& size);
void calculTableau(int* t, int size, int& min, int& max, double& moy);
void menu();

// test de classe de mémorisation
void testMemStatic();
void testMemExtern();

// Exercices chaine de caractère
std::string inverser(const std::string &str);
bool palindrome(const std::string &str);

#endif