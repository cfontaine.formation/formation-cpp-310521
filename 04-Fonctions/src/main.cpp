#include <iostream>
#include "MesFonctions.h"
#include "MesStructures.h"

using namespace std;

// Déclaration de la fonction => Déplacer dans MesFonctions.h
// int somme(int ,int);
// void afficher(int a);

const int testVarExtern = 123;
static double pi = 3.14; // avec static la variable n'est visible que dans ce fichier

int main(int argc, char *argv[])
{
    // Appel de methode
    cout << somme(1, 2) << endl;

    // Appel de methode (sans retour)
    afficher(23);

    // Paramètre passé par valeur
    // C'est une copie de la valeur du paramètre qui est transmise à la méthode
    int p = 1;
    testParamValeur(p);
    cout << p << endl; //1

    // Paramètre par défaut (uniquement pour les paramètres passés par valeur)
    testParamDefault(1.2);
    testParamDefault(3.6, 10);
    testParamDefault(4.1, 11, 'z');

    // Exercice Fonction maximum
    cout << "Entrer 2 nombres" << endl;
    int a, b;
    cin >> a >> b;
    cout << "Maximum=" << maximum(a, b) << endl;

    // Exercice Fonction parité
    cout << "Entrer un nombre entier" << endl;
    int v;
    cin >> v;
    cout << even(v) << endl;

    // Paramètre passé par adresse
    // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
    testParamAdresse(&p); // on passe en paramètre l'adresse de la variable p
    cout << p << endl;

    // Les tableaux peuvent être passer par adresse et pas par valeur
    int tab[] = {1, 2, 3, 7, 2};
    afficherTableau(tab, 5); // le nom du tableau correspond à un pointeur sur le premier élément du tableau
    afficherTableau2(tab, 5);

    // Paramètre passé par référence
    cout << p << endl;
    testParamReference(p);
    cout << p << endl;
    // testParamReference(3);

    // avec une référence constante, on peut aussi passer des litérals
    testParamReferenceCst(3);
    testParamReferenceCst(p);
    testParamReferenceCst();

    // Fonction à nombre variable de paramètre  (hérité du C)
    cout << moyenne(3, 5, 7, 2) << endl;
    cout << moyenne(1, 5) << endl;
    cout << moyenne(5, 5, 0, 6, 7, 6) << endl;

    // Fonction à nombre variable de paramètre  (C++11)
    cout << moyenneC11({4, 5}) << endl;
    cout << moyenneC11({}) << endl;
    cout << moyenneC11({4, 4, 6, 7}) << endl;

    // Fonction inline
    // L'appel d'une fonction iniline peut-être remplacé par la valeur évaluée lors de la compilation
    int d = doubler(6); // remplacer par d=12;
    cout << d << endl;
    int fi = 11;
    cout << doubler(fi) << endl; // remplacer par 22
    cin >> fi;
    cout << doubler(fi) << endl; // ne peut pas être remplacé à la compilation, la fonction se comporte comme une fonction normale

    // Surcharge de fonction
    cout << multiplier(1, 2) << endl;
    cout << multiplier(1.7, 52.7) << endl;
    cout << multiplier(7, 2.6) << endl;
    cout << multiplier(1, 2, 5) << endl;
    cout << multiplier('\02', '\04') << endl; // Convertion des caractères en entier
    // cout << multiplication(5.3,2) << endl;		// Erreur, il n' y a pas de fonction multiplication avec pour paramètre un double et un entier

    // Paramètre de la fonction main
    for (int i = 0; i < argc; i++)
    {
        cout << argv[i] << endl;
    }

    // Fonction recursive
    int res = factorial(3);
    cout << res << endl;

    // /!\ une fonction ne doit pas retourner une référence ou un pointeur sur une variable locale à une fonction
    int *ptr = testRetour();
    cout << ptr << " " << *ptr << endl;
    delete ptr;

    // Exercice  fonction tableau
    menu();

    // Classe de mémorisation
    testMemStatic();
    testMemStatic();
    testMemExtern();

    // struct Contact c1; // en C
    Contact c1; // en C++ struct n'est pas nécéssaire
    c1.prenom = "John";
    c1.nom = "Doe";
    c1.tel = "03.20.00.00.00";
    c1.age = 60;

    cout << c1.nom << endl;
    c1.afficher();

    Contact c2 = {"Jane", "Doe", "06.20.00.00.00", 37};

    c2.afficher();

    Contact c3 = c1;
    c3.afficher();
    c3.age = 25;
    c3.afficher();
    c1.afficher();

    // On teste l'égalité champs par champs
    bool testEquals = c1.prenom == c3.prenom && c1.nom == c3.nom && c1.age == c3.age && c1.tel == c3.tel;

    Contact *ptrC = new Contact;
    (*ptrC).nom = "Smithee";
    ptrC->prenom = "alan";
    ptrC->age = 49;
    ptrC->tel = "03.27.00.00.00";
    ptrC->afficher();
    delete ptrC;

    const Contact cCst = {"Joe", "Dalton", "06.20.00.00.00", 37};
    cout << cCst.age << endl;
    cCst.age = 54; // champs mutable => on peut le modifier même si l'objet est constant
    // cCst.prenom="Jack";

    // Chaine de caractère C
    // en C, un chaine de caractère est un tableau de caractère qui est terminé par un caractère terminateur '\0'
    const char *strC = "Bonjour"; // Chaine constante => const char*
    cout << strC << endl;
    cout << sizeof(strC) << endl; // Nombre de caractères de la chaine +1 pour le terminateur \0

    char strC2[] = "HelloWorld";
    strC2[0] = 'h';

    // en C++ => string
    string str = "Hello";
    cout << str << endl;
    string strA = string("bonjour");
    string strB = string(10, 'a');
    cout << strA << " " << strB << endl;

    // Concaténation
    str += " World"; // Opérateur + et += cocaténation de chaine
    cout << str << endl;
    str.append(" !!!!!"); // append => permet d'ajouter la chaine passée en paramètre en fin de chaine
    cout << str << endl;
    str.push_back('?'); // push_back=> ajoute un le caractère passé en paramètre en fin de chaine
    cout << str << endl;

    //Comparaison
    // On peut utiliser les opérateurs de comparaison avec les chaines de caractères
    if (strA == strB)
    {
        cout << "==" << endl;
    }
    else
    {
        cout << "!=" << endl;
    }

    if (strB < strA)
    {
        cout << "Inferieur" << endl;
    }

    cout << str.length() << endl; // length => nombre de caractères de la chaine (idem pour size)
    cout << str.empty() << endl;  // empty => retourne true si la chaine est vide

    // Accès à un cararactère de la chaine [] ou at
    cout << str[1] << endl;
    cout << str.at(1) << endl;
    //  cout<<str[40]<<endl;
    //   cout<<str.at(40)<<endl; // avec at si l'indice dépasse la taille de la chaine une exception est lancée

    str[0] = 'h';
    cout << str << endl;

    // Extraire une sous-chaine
    cout << str.substr(5) << endl;    // substr => retourne une  sous-chaine qui commence à l'indice 5 et jusqu'à la fin
    cout << str.substr(6, 5) << endl; // substr => retourne une  sous-chaine de 5 caractères qui commence à l'indice 6

    str.insert(5, "-----"); // Insère la chaine passé en paramètre à la position 5
    cout << str << endl;

    str.erase(5, 5); // Supprimer 3 caractères de la chaine à partir de la position 5
    cout << str << endl;
    str.erase(12); // supprimer les caractères de la chaine à partir de la position 12 jusqu'à la fin de la chaine
    cout << str << endl;

    str.replace(2, 1, "_"); // replace => remplace à la position 5 , 1 caractère à partir de la chaine passée en paramètre
    cout << str << endl;

    // Recherche de la position de la chaine ou du caractère passée en paramètre dans la chaine str
    cout << str.find("World") << endl;                        // à partir du début de la chaine
    cout << str.find('o') << endl;                            // à partir du début de la chaine
    cout << str.find('o', 5) << endl;                         // à partir de la position 5
    cout << str.find('o', 8) << "  " << string::npos << endl; // à partir de la position 8, pas de caractère => retourne la valeur std::string::npos

    cout << str.rfind('o') << endl; // rfind => idem find, mais on commence à partir de la fin de la chaine et on va vers le debut

    const char *strcCnv = str.c_str(); // convertion string en chaine C (const *char)
    cout << strcCnv << endl;

    // Iterateur = > objet qui permet de parcourir la chaine(un peu comme un pointeur)
    // begin => retourne un iterateur sur le début de la chaine
    // end => retourne un iterateur sur la fin de la chaine
    for (string::iterator it = str.begin(); it != str.end(); it++) // it++ permet de passer au caractère suivant
    {
        cout << *it << endl; // *it permet d'obtenir le caractère "pointer" par l'itérateur
        //*it='a'; // on a accés en lecture et en écriture
    }

    // rbegin et rend idem  mais l'itérateur part de la fin et va vers le début de la chaine
    for (auto it = str.rbegin(); it != str.rend(); it++) // it++ permet de passer au caractère précédent
    {
        cout << *it << endl;
    }

    for (char c : str) // C++11
    {
        cout << c << endl;
    }

    // Différence rbegin / crbegin
    // crbegin (C++11) => on a accès uniquement en lecture par l'intermédiaire de l'ittérateur
    for (auto it = str.cbegin(); it != str.cend(); it++)
    {
        cout << *it << endl;
        // *it='a';
    }

    str.clear();                 // Efface les caractères contenus dans la chaine
    cout << str.empty() << endl; // empty => retourne true si la chaine est vide

    // Exercice
    cout << "Inverser=" << inverser("Bonjour") << endl;
    cout << palindrome("radar") << endl;
    cout << palindrome("bonjour") << endl;
}

// Définition de la fonction => Déplacer dans MesFonctions.cpp
// int somme(int v1, int v2)
// {
//     return v1 + v2;
// }

// void afficher(int a)
// {
//     cout<<a<<endl;
//     //return;
// }