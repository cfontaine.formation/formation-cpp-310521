#include <iostream>
#include <cstdarg>
#include "MesFonctions.h"

using namespace std;

// Fichier .cpp contient les Définitions des fonctions

int somme(int v1, int v2)
{
    return v1 + v2;
}

void afficher(int a)
{
    cout << a << endl;
    //return;
}

void testParamValeur(int a)
{
    cout << "a=" << a << endl;
    a = 42;
    cout << "a=" << a << endl;
}

// Les paramètres par défaut sont uniquement défini dans la déclaration de la fonction (.h)
void testParamDefault(double d, int i, char c)
{
    cout << d << " " << i << " " << c << endl;
}

double maximum(double a, double b)
{
    return a > b ? a : b;
}

bool even(int val)
{
    return val % 2 == 0;
}

// En utilisant le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre
void testParamAdresse(int *v)
{
    cout << *v << endl;
    *v = 42;
    cout << *v << endl;
}

void afficherTableau(int *t, int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << t[i] << " " << endl;
    }
}

void afficherTableau2(int t[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << t[i] << " " << endl;
    }
}

// Passage de paramètre par référence
// Comme avec le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre
void testParamReference(int &a)
{
    cout << a << endl;
    a = 456;
}

void testParamReferenceCst(const int &v)
{
    cout << v << endl;
    //v=1;
}

// Nombre d'arguments variable
// en C et c++98
double moyenne(int nbArgs, ...)
{
    va_list vaList;
    double somme = 0;
    va_start(vaList, nbArgs);
    for (int i = 0; i < nbArgs; i++)
    {
        somme += va_arg(vaList, int);
    }
    va_end(vaList); // nettoyage des arguments
    return nbArgs == 0 ? 0.0 : somme / nbArgs;
}

// C++11
double moyenneC11(initializer_list<int> varg)
{
    double somme = 0;
    for (auto v : varg)
    {
        somme += v;
    }
    if (varg.size() == 0)
    {
        return 0.0;
    }
    else
    {
        return somme / varg.size();
    }
}

// Surcharge de fonction
// Plusieurs fonctions peuvent avoir le même nom
// Ces arguments doivent être différents en nombre ou/et en type pour qu'il n'y ai pas d'ambiguité pour le compilateur
int multiplier(int a, int b)
{
    cout << "2 entiers" << endl;
    return a * b;
}

double multiplier(double a, double b)
{
    cout << "2 doubles" << endl;
    return a * b;
}

double multiplier(int a, double b)
{
    {
        cout << "un entier un double" << endl;
        return a * b;
    }
}

int multiplier(int a, int b, int c)
{
    cout << "3 entiers" << endl;
    return a * b * c;
}

int factorial(int n) // factoriel= 1* 2* … n
{
    if (n <= 1) // condition de sortie
    {
        return 1;
    }
    else
    {
        int p = n * 2;
        return factorial(n - 1) * n;
    }
}

int *testRetour()
{
    int a = 10;
    //int b=a+4; // variable locale
    int *b = new int;
    *b = a + 4;
    //return &b;
    return b;
}

// Exercice Tableau
// - Écrire un méthode qui affiche un tableau d’entier
// - Écrire une méthode qui permet de saisir :
//		- La taille du tableau
//		- Les éléments du tableau
// - Écrire une méthode qui calcule :
//		- le minimum
//		- le maximum
//		- la moyenne
int *saisirTableau(int &size)
{
    cout << "Saisir la taille du tableau" << endl;
    cin >> size;
    if (size <= 0)
    {
        size = 0;
        return nullptr;
    }

    int *t = new int[size];
    for (int i = 0; i < size; i++)
    {
        cout << "t[" << i << "]= ";
        cin >> t[i]; // correspond à *(*t+i)
    }
    return t;
}

void calculTableau(int *t, int size, int &min, int &max, double &moy)
{
    max = t[0];
    min = t[0];
    double somme = 0;
    for (int i = 0; i < size; i++)
    {
        if (t[i] > max)
        {
            max = t[i];
        }
        if (t[i] < min)
        {
            min = t[i];
        }
        somme += t[i];
    }
    moy = somme / size;
}

void afficherMenu()
{
    cout << "1 - Saisir le tableau" << endl;
    cout << "2 - Afficher le tableau" << endl;
    cout << "3 - Afficher le minimum, le maximum et la moyenne" << endl;
    cout << "0 - Quitter" << endl;
}

void menu()
{
    int *tab = 0;
    int size = 0;
    int choix;
    afficherMenu();
    do
    {
        cout << "Choix= ";
        cin >> choix;
        switch (choix)
        {
        case 1:
            delete[] tab;
            tab = saisirTableau(size);
            break;

        case 2:
            if (tab != 0)
            {
                afficherTableau(tab, size);
            }
            else
            {
                cout << "Il n'y a pas de tableau saisie" << endl;
            }
            break;

        case 3:
            if (tab != 0)
            {
                int min, max;
                double moy;
                calculTableau(tab, size, min, max, moy);
                cout << "minimum= " << min << " maximum=" << max << " moyenne=" << moy << endl;
            }
            else
            {
                cout << "Il n'y a pas de tableau saisie" << endl;
            }

            break;
        case 0:
            cout << "Au revoir!" << endl;
            break;
        default:
            cout << "choix non disponnible" << endl;
        }
    } while (choix != 0);
    delete[] tab;
}

// Classe de mémorisation static
void testMemStatic()
{
    // Entre 2 exécutions a conserve sa valeur
    static int a = 903; // par défaut initialisé à 0
    a++;
    cout << a << endl;
}

// Classe de mémorisation extern
void testMemExtern()
{
    cout << testVarExtern << endl;
}

// Exercice Inversion de chaine
// Écrire la fonction inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
string inverser(const string &str)
{
    string tmp = "";
    for (auto it = str.rbegin(); it != str.rend(); it++)
    {
        tmp.push_back(*it);
    }
    return tmp;
}

//  Exercice Palindrome
// Écrire une méthode Palindrome qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
bool palindrome(const std::string &str)
{
    return str == inverser(str);
}