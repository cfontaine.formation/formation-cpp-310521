#ifndef MES_STRUCTURES_H
#define MES_STRUCTURES_H
#include <string>

struct Contact
{
    // Champs
    std::string prenom;
    std::string nom;
    std::string tel;
    mutable int age; // mutable => si la structure est constante,
                     // on pourra quand même modifier les champs mutable

    //Fonction
    void afficher(); // En c++ une structure peut contenir des fonctions
};

#endif