#include <iostream>
#include "Pair.h"
#include <string>

using namespace std;

template<typename T> T somme(T a, T b)
{
    return a+b;
}


int main()
{
    Pair<int> p1(1,2);
    cout<<p1.maximum()<<endl;
    cout<<p1.minimum()<<endl;

    Pair<string> pstr("azerty","bonjour");
    cout<<pstr.maximum()<<endl;

    cout<<somme<int>(1,2)<<endl;
    cout<<somme(string("aze"),string("rty"))<<endl;
    cout<<somme(3.6F,4.5F)<<endl;
}