#include <iostream>
#include <climits>

using namespace std;

// en c++ 98
enum direction
{
    NORD = 90,
    EST = 0,
    OUEST = 180,
    SUD = 270
};

// en C++11
enum class motorisation
{
    ESSENCE,
    DIESEL,
    GPL,
    ELECTRIQUE = 20,
    HYDROGENE
};

int main()
{
      // tableau à une dimension
    // Déclaration: type nom[taille]
    int tab[5];

    // Initialisation du tableau
    for (int i = 0; i < 5; i++)
    {
        tab[i] = 0;
    }

    // Accès à un élément du tableau
    tab[1] = 3;
    cout << "tab[1]=" << tab[1] << endl;

    // Déclaration et initialisation d'un tableau
    char tabChr[] = {'a', 'z', 'e', 'r'};

    // Parcourir un tableau
    for (int i = 0; i < 4; i++)
    {
        cout << tabChr[i] << endl;
    }

    // Parcourir un tableau en C++11
    // Pas de modification des éléments du tableau
    for (auto c : tabChr) //char
    {
        cout << c << endl;
    }

    //  En utilisant une référence, on peut  modifier des éléments du tableau
    for (auto &c : tabChr)
    {
        cout << c << endl;
        c = 'T';
    }

    // Calculer la taille d'un tableau (ne fonctionne pas avec les tableaux passés en paramètre de fonction)
    cout << sizeof(tabChr) / sizeof(tabChr[0]) << endl;

    // Exercice : tableau
    // Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7, 4, 8, 0, -3
    int t[] = {-7, 4, 8, 0, -3};

    int max = INT_MIN; // max=t[0]
    double somme = 0.0;
    for (int i = 0; i < 5; i++)
    {
        if (t[i] > max)
        {
            max = t[i];
        }
        somme += t[i];
    }
    cout << "Maximum=" << max << " Moyenne=" << somme / 5 << endl;

    // Tableau à 2 dimensions
    // Déclaration
    double tab2D[3][2];

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            tab2D[i][j] = 0.0;
        }
    }

    // Accès à un élément
    tab2D[0][1] = 3.7;
    cout << tab2D[0][1] << endl;

    // Calculer le nombre  de colonnne  d'un tableau (ne fonctionne pas avec les tableaux passés en paramètre de fonction)
    int nbColonne = sizeof(tab2D[0]) / sizeof(tab2D[0][0]);
    cout << nbColonne << endl;
    // Calculer le nombre  de ligne  d'un tableau
    int nbLigne = sizeof(tab2D) / sizeof(tab2D[0]);
    cout << nbLigne << endl;
    // Calculer le nombre  d'élément'  d'un tableau
    cout << (sizeof(tab2D) / sizeof(tab2D[0][0])) << endl; // nb élément du tableau

    // Déclaration et initialisation
    int tab2Di[3][2] = {{1, 3}, {2, 4}, {5, 6}}; // ou  { 1,3,3,5,3,8 };

    // Parcourir le tableau
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            cout << tab2Di[i][j];
        }
        cout << endl;
    }

    // Pointeur
    double val = 12.34;

    // Déclaration d'un pointeur de double
    double *ptr;

    ptr = &val; // &val => adresse de la variable val
    cout << "val=" << val << "ptr=" << ptr << endl;

    // *ptr => Accès au contenu de la variable pointer par ptr
    cout << *ptr << endl;
    *ptr = 67.89;
    cout << *ptr << "val=" << val << endl;

    // Un pointeur qui ne pointe sur rien
    double *ptr2 = 0;       // 0 en C++ (correspond à NULL en C)
    double *ptr3 = nullptr; // nullptr en C++11

    //  On peut affecter un pointeur avec un pointeur du même type
    ptr2 = ptr; // ptr2 contient aussi l'adresse de val
    cout << *ptr2 << endl;

    // On peut comparer 2 pointeurs du même type ou compare un pointeur à 0 ou nullptr
    if (ptr3 == 0) // ou ptr3==nullptr en C++ 11
    {
        cout << "le pointeur est null" << endl;
    }

    if (ptr == ptr2)
    {
        cout << "pointeur egaux" << endl;
    }

    // Pointeur constant
    // En préfixant un pointeur avec const => Le contenu de la variable pointée est constant
    const double *ptrCst = &val;
    cout << *ptrCst << endl;
    //*ptrCst=7.8;   // on ne peut plus modifier le contenu pointé par l'intermédiaire du pointeur
    ptrCst = 0; // Mais on peut modifier le pointeur

    // En postfixant un pointeur avec const => le pointeur est constant
    double *const ptrCst2 = &val;
    *ptrCst2 = 8.9;
    cout << *ptrCst2 << endl;
    //ptrCst2=ptr3;     // Le pointeur ptrCst2  est constant, on ne peut plus le modifier

    // En préfixant et en postfixant un pointeur avec const: le pointeur pCst est constant et
    // l'on ne peut plus modifier le contenu de la variable pointée par l'intermédiaire du pointeur
    const double *const ptrCst3 = &val;
    cout << *ptrCst3 << endl;
    //*ptrCst3=1.2;
    ///ptrCst3=ptr3;

    // Opérateur const_cast
    ptrCst = &val;
    double *ptr4 = const_cast<double *>(ptrCst);    // en C++, const_cast permet de supprimer le qualificatif const
                                                    // en C (int*)pCpre;
    *ptr4 = 3.4;                                       
    cout << *ptr4 << "val=" << val << endl;

    // Tableau et Pointeur
    int t2[] = {3, 6, 4, -3};
    int *ptrTab = t2;           // Le nom d'un tableau est un pointeur sur le premère élément
    cout << *ptrTab << endl;    // On accède au premier élément du tableau

    // On accède au deuxième élément du tableau
    cout << *(ptrTab + 1);      // équivalent à ptrTab + sizeof(int) ou à t2[1]
    cout << ptrTab[1] << endl;  // on peut accèder à un tableau avec un pointeur comme à un tableau statique avec l'opérateur []

    ptrTab++;
    cout << *ptrTab << endl;    // équivalent à t2[1]

    // poiteur void => C
    void *ptrVoid = ptrTab;
    //double* ptrInt=(double*)ptrVoid;
    //cout<<*ptrInt<<endl;
    int *ptrInt = (int *)ptrVoid;
    cout << *ptrInt << endl;

    // reinterpret_cast
    int str = 0X61 | 0X64 << 8; // 00 00 64 61
    int *ptrStr = &str;
    cout << hex << *ptrStr << endl;
    char *ptrC = reinterpret_cast<char *>(ptrStr);
    cout << *ptrC << endl;       // 61 =>a
    cout << *(ptrC + 1) << endl; // 64 => d

    // Allocation dynamique
    double *ptrDyn = new double;    // new => allocation de la mémoire
    *ptrDyn = 12.34;
    cout << *ptrDyn << " " << ptrDyn << endl;
    delete ptrDyn;                  // delete => libération de la mémoire
    cout << ptrDyn << endl;
    ptrDyn = 0; //nullptr           

    // Allocation dynamique avec initialisation
    ptrDyn = new double(67.56);
    cout << *ptrDyn << endl;
    delete ptrDyn;

    ptrDyn = new double{656.90}; // en C++11
    cout << *ptrDyn << endl;
    delete ptrDyn;

    // Allocation dynamique d'un tableau
    int s = 5;
    char *ptrTabDyn = new char[s];      // new [] => création d'un tableau dynamique
    *ptrTabDyn = 'a';       // ptrTabDyn[0]='a';
    *(ptrTabDyn + 1) = 'b'; // ptrTabDyn[1]='b';
    // On peut accèder à un tableau dynamique comme à un tableau statique
    cout << *ptrTabDyn << " " << ptrTabDyn[0] << endl;
    cout << ptrTabDyn[1] << endl;
    delete[] ptrTabDyn;     // delete [] => libération d'un tableau dynamique
    ptrTabDyn = 0;

    // Exercice Pointeur
    double v1 = 3, v2 = 4, v3 = 10;
    double *ptrChoix = 0;

    int choix;
    cin >> choix;
    switch (choix)
    {
    case 1:
        ptrChoix = &v1;
        break;
    case 2:
        ptrChoix = &v2;
        break;
    case 3:
        ptrChoix = &v3;
        break;
    }
    if (ptrChoix != 0)
    {
        cout << *ptrChoix << endl;
    }

    // Exercice : Tableau dynamique
	// Modifier le programme Tableau pour faire la saisie :
	// - de la taille du tableau
	// - des éléments du tableau
	// Trouver la valeur maximale et la moyenne du tableau
    int size;
    cin >> size;
    if (size > 0)
    {
        int *ptrT = new int[size];

        for (int i = 0; i < size; i++)
        {
            cout << "t[" << i << "]=";
            cin >> ptrT[i];
        }

        int maximum = ptrT[0];
        double somme2 = 0.0;
        for (int i = 1; i < size; i++)
        {
            if (ptrT[i] > maximum)
            {
                maximum = ptrT[i];
            }
            somme2 += ptrT[i];
        }
        cout << "Maximum=" << maximum << " Moyenne=" << somme2 / size << endl;
        delete[] ptrT;
    }

    // Référence 
	// Une référence correspond à un autre nom que l'on donne à la variable
	// on doit aussi intialiser une référence avec une lvalue (= qui possède une adresse)
    int b = 123;
    int &ref = b;
    ref = 56;
    cout << ref << endl;
    // int &r2 =56;     // Erreur pas d'initialisation

    // Référence constante
    const int &refCst = b;
    cout << refCst << endl; 
    //refCst=34;        // On ne peut plus utiliser une référence constante pour modifier la valeur
   
    const int &refCst2 = 45;    // On peut initialiser une référence constante avec une valeur litérale
    cout << refCst2 << endl;

    // énumération
    // Convertion implicite enum -> int
    direction dir = SUD; //direction::SUD
    int vDir = dir;
    cout << dir << " " << vDir << endl;

    // int => enum
    vDir = 90;
    switch (vDir)
    {
    case 0:
        dir = EST;
        break;
    case 90:
        dir = NORD;
        break;
    case 270:
        dir = SUD;
        break;
    case 180:
        dir = OUEST;
        break;
        //default:
        // générer une exception
    }

    // avec enum class en c++ 11
    motorisation mt = motorisation::DIESEL;
    // avec enum class, il n'y a plus de convertion implicite enum -> int
    int vMt = static_cast<int>(mt);     // il faut utiliser un static_cast
    cout << vMt << endl;

    // On peut utiliser une énumération dans un switch
    switch (mt)
    {
    case motorisation::DIESEL:
        cout << "Voiure diesel" << endl;
        break;
    case motorisation::ESSENCE:
        cout << "Voiure essence" << endl;
        break;
    default:
        cout << "Autre motorisation" << endl;
    }
}