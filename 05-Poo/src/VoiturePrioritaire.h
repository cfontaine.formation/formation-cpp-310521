#ifndef VOITURE_PRIORITAIRE_H
#define VOITURE_PRIORITAIRE_H

#include "Voiture.h"

class VoiturePrioritaire : public Voiture
{
    bool gyro;

public:
    VoiturePrioritaire (): gyro(false){
        std::cout<<"Constructeur par défaut de voiture prioritaire"<<std::endl;
    }

    VoiturePrioritaire(bool gyro): Voiture("Toyota","Jaune","fr-5645-ER"), gyro(gyro)
    {

    }

    
    VoiturePrioritaire(std::string marque,std::string couleur, std::string plaqueIma,bool gyro): Voiture(marque,couleur,plaqueIma), gyro(gyro)
    {

    }
    void allumerGyro();
    void eteindreGyro();
    bool estAllumer() const
    {
       // vitesse=10;
        return gyro;
    }
    void afficher() const;
};

#endif 