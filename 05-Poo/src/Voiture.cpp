#include "Voiture.h"
#include <iostream>

using namespace std;

// Initialisation de la variable de classe
int Voiture::cptVoiture = 0;

// Voiture::Voiture()
// {
//     marque="Opel";
//     couleur="Noir";
//     plaqueIma="ww-wwww-ww";
//     vitesse=0;
//     compteurKm=0;
// }

// Voiture::Voiture(std::string marque,std::string couleur,std::string plaqueIma)
// {
//     this->marque=marque;
//     this->couleur=couleur;
//     this->plaqueIma=plaqueIma;
//     vitesse=0;
//     compteurKm=10;
// }

 Voiture::~Voiture()
 {
    delete moteur;
    cout<<"Destructeur"<<endl;
 }

// Constructeur par copie
Voiture::Voiture(const Voiture &v)
{
    marque = v.marque;
    couleur = v.couleur;
    plaqueIma = v.plaqueIma;
    vitesse = v.vitesse;
    compteurKm = v.compteurKm;
    proprietaire=v.proprietaire;
    moteur=new Moteur (*v.moteur);
    cptVoiture++;
    //cout<<"Constructeur par copie"<<endl;
}

void Voiture::accelerer(int vAcc)
{
    if (vAcc > 0)
    {
        vitesse += vAcc;
    }
}

void Voiture::freiner(int vFrn)
{
    if (vFrn > 0)
    {
        vitesse -= vFrn;
    }
}

void Voiture::arreter()
{
    vitesse = 0;
}
bool Voiture::estArreter() const
{
    return vitesse == 0;
}

void Voiture::afficher() const
{
    cout << marque << " " << couleur << " " << plaqueIma << " " << vitesse << " " << compteurKm << endl;
    if(proprietaire!=0)
    {
        proprietaire->afficher();
    }
}

void Voiture::testMethodeClasse()
{
    cout << "Methode de classe" << cptVoiture << endl;
    //cout << vitesse << endl;	// dans une méthode de classe,e on ne peut pas accèder au variables d'instances
    //arreter();				// ni aux méthodes d'instances
}

bool Voiture::compareVitesse(const Voiture &va, const Voiture &vb)
{
    // Dans un méthode de classe, on peut uniquement accèder à une variable d'instance
    // par l'intermédiaire d' objet passé en paramètre
    return va.vitesse == vb.vitesse;
}