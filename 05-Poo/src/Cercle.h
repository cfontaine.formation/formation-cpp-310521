#ifndef CERCLE_H
#define CERCLE_H
#include "Point.h"
class Cercle
{
    Point centre;
    double rayon;
public:
    Cercle(const Point &centre,double rayon): centre(centre),rayon(rayon)
    {

    }

    Point getCentre() const
    {
        return centre;
    }

    double getRayon() const
    {
        return rayon;
    }


    bool colision(const Cercle &c1);
    static bool colision(const Cercle &c1,const Cercle &c2);

    static bool contenir(Cercle c1,Point p);

    bool contenir(Point p);

};

#endif 