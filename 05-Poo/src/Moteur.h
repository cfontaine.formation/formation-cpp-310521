#ifndef MOTEUR_H

class Moteur{
    int puissance;
public:
    Moteur(int puissance=10) : puissance(puissance){     }

    int getPuissance() const
    {
        return puissance;
    }

    void afficher() const;
};

#endif 