#ifndef COMPTE_EPARGNE
#define COMPTE_EPARGNE
#include "CompteBancaire.h"

class CompteEpargne : public CompteBancaire
{
	double taux;

public:
	CompteEpargne(std::string titulaire, double solde, double taux) : CompteBancaire(titulaire, solde), taux(taux)
	{
	}

	void calculInterets();

};

#endif 