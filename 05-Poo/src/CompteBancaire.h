#pragma once
#include <string>

class CompteBancaire
{
    protected:
    double solde;
    private:
    std::string iban;
    std::string titulaire;

    static int cptCompte;
public:
    CompteBancaire(std::string titulaire) : titulaire(titulaire), solde(0.0)
    {
        cptCompte++;
        iban = "fr-5962-0000-" + std::to_string(cptCompte);
    }

    CompteBancaire(std::string titulaire, double solde) : titulaire(titulaire), solde(solde)
    {
        cptCompte++;
        iban = "fr-5962-0000-" + std::to_string(cptCompte);
    }

    std::string getIban() const
    {
        return iban;
    }

    double getSolde() const
    {
        return solde;
    }

    std::string getTitulaire() const
    {
        return titulaire;
    }

    void setTitulaire(const std::string &titulaire)
    {
        this->titulaire=titulaire;
    }

    static int getcptCompte()
    {
        return cptCompte;
    }

    void afficher() const;
    void crediter(double valeur);
    void debiter(double valeur);
    bool estPositif() const;
};