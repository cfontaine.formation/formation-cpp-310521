#include <iostream>
#include "Voiture.h"
#include "CompteBancaire.h"
#include "Point.h"
#include "Cercle.h"
#include "VoiturePrioritaire.h"
#include "CompteEpargne.h"
using namespace std;

int main()
{
  cout << "cpt=" << Voiture::getCptVoiture()<< endl; //cptVoiture 
  Voiture::testMethodeClasse();   // Appel d'une méthode de classe

  // Instanciation statique
  Voiture v1("Fiat", "Jaune", "fr-455-AB");
  cout << "cpt=" <<Voiture::getCptVoiture() << endl;
  v1.setVitesse(10);  // vitesse= 10;

  // Appel dune méthode d'instance
  v1.accelerer(20);
  cout << v1.getVitesse()<< endl; //vitesse 
  v1.freiner(10);
  cout << v1.getVitesse() << endl; //vitesse 
  cout << v1.estArreter() << endl;
  v1.arreter();
  cout << v1.estArreter() << endl;
  v1.afficher();
  v1.getMoteur().afficher();

  // Instanciation dynamique
  Voiture *v2 = new Voiture();
  cout << "cpt=" << Voiture::getCptVoiture()<< endl;
 v2->setVitesse(20);// v2->vitesse = 20;
  cout << v2->getVitesse() << endl;
  v2->accelerer(10);
  v2->afficher();
  delete v2;

  // Appel du constructeur par copie
  Voiture v3 = Voiture(v1);
  // Voiture v3=v1;
  cout << "cpt=" << Voiture::getCptVoiture() << endl;
  v3.afficher();
  cout << Voiture::compareVitesse(v1, v3) << endl;

  Personne per1("John","Doe");
  Voiture v4("honda","blanc","fr-3424-DF",per1);
  // Personne* tmp=v4.getProprietaire();
  // tmp->afficher();
  v4.afficher();
  v4.getProprietaire()->afficher();
  


  // Exercice CompteBancaire
  CompteBancaire cb("John Doe", 100.0);
  // cb.solde=100.0;
  //cb.iban="fr-5962-0000-000";
  //  cb.titulaire="John Doe";
  cb.afficher();
  cb.debiter(50);
  cb.afficher();
  cout << cb.estPositif() << endl;
  cb.debiter(150);
  cout << cb.estPositif() << endl;
  cout<<cb.getSolde()<<endl;


  // Point
  Point p1;
  p1.deplacer(2,2);
  p1.afficher();

  Point* p2 =new Point(3,2);
  p2->afficher();
  cout<<p2->norme()<<endl;
  cout<<Point::distance(p1,*p2)<<endl;
  delete p2;

  // cercle
  Point pc=Point(2,0);
  Cercle c1(pc,2.0);
  Cercle c2(Point(6,0),1.0);
  Cercle c3(Point(6,0),3.0);
  cout<<Cercle::colision(c1,c3)<<endl;
  cout<<c1.colision(c3)<<endl;
   cout<<c1.colision(c2)<<endl;
   Point po(10,10);
   Point pi(1,1);
   cout<<Cercle::contenir(c1,po)<<endl;
   cout<<c1.contenir(pi)<<endl;

   // Héritage 
  VoiturePrioritaire vp1= VoiturePrioritaire(true);
  vp1.accelerer(20);
  vp1.allumerGyro();
  cout<<vp1.estAllumer()<<endl;
  vp1.afficher();

  VoiturePrioritaire vp2=VoiturePrioritaire("Renault","rouge","az-4584-FV",true);
  vp2.afficher();

  // Compte épargne Exercice héritage
  CompteEpargne ce("John Doe",200.0,2.0);
  ce.afficher();
  ce.calculInterets();
  ce.afficher();

  Voiture* vp3=new VoiturePrioritaire(true);
  vp3->afficher();

}