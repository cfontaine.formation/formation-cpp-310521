#ifndef PERSONNE_H
#define PERSONNE_H
#include <string>

class Personne
{
    std::string prenom;
    std::string nom;

public:
    Personne(std::string prenom, std::string nom) : prenom(prenom), nom(nom) {}

    std::string getPrenom() const
    {
        return prenom;
    }
    std::string getNom() const
    {
        return nom;
    }

    void setPrenom(std::string prenom)
    {
        this->prenom = prenom;
    }

    void setNom(std::string nom)
    {
        this->nom = nom;
    }

    void afficher() const;
};

#endif