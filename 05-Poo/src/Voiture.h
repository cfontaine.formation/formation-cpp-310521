#ifndef VOITURE_H

#define VOITURE_H
#include <string>
#include <iostream>
#include "Personne.h"
#include "Moteur.h"
class Voiture
{
    // par defaut private

    // Variable d'instance
    std::string marque;
    std::string couleur;
    std::string plaqueIma;
    int vitesse;
    int compteurKm;

    // Agrégation
    Personne* proprietaire;

//composition
    Moteur *moteur;

    // Variable de classe
    static int cptVoiture;

public:
    // Constructeurs
    // Constructeur défaut
    Voiture() : marque("Opel"), couleur("noir"), plaqueIma("ww-wwww-ww"), vitesse(0), compteurKm(0),proprietaire(0)
    {
        moteur=new Moteur();
        cptVoiture++;
        std::cout<<"Constructeur par défaut Voiture"<<std::endl;
    }

    // On peut surcharger les constructeurs
    Voiture(std::string marque, std::string couleur, std::string plaqueIma) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(0), compteurKm(10),proprietaire(0)
    {
         moteur=new Moteur();
        cptVoiture++;
    }
    // Voiture(std::string marque, std::string couleur, std::string plaqueIma, Personne *proprietaire) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(0), compteurKm(10),proprietaire(proprietaire)
    Voiture(std::string marque, std::string couleur, std::string plaqueIma, Personne &proprietaire) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(0), compteurKm(10),proprietaire(&proprietaire)
    {
         moteur=new Moteur();
        cptVoiture++;
    }

    // Constructeur par recopie
    // Il n'est pas nécessaire de faire un constructeur par copie, si l'on n'a pas d'attribut alloué dynamiquement (pointeur)
    // si on veut supprimer la possibilité de faire une copie de l'objet, on peut:
    // - uniquement le  déclarer et ne pas donner de définition
    // - le rendre privé
    Voiture(const Voiture &v);

    // Destructeur
    // Le destructeur est appelé juste avant la destruction de l'objet
    // Il est principalement utilisé pour libérer des ressources.
    // si l'on n'a pas de ressource à libérer, on n'a pas besoin de le définir
     ~Voiture();

    // Getter et Setter

    int getVitesse() const
    {
        return vitesse;
    }

    void setVitesse(int vitesse)
    {
        if (vitesse >= 0)
        {
            this->vitesse = vitesse;
        }
    }

    std::string getCouleur() const
    {
        return couleur;
    }

    void setCouleur(const std::string &couleur)
    {
        this->couleur = couleur;
    }

    std::string getMarque() const
    {
        return marque;
    }

    std::string getPlaqueIma() const
    {
        return plaqueIma;
    }

    void setPlaqueIma(const std::string &plaqueIma)
    {
        this->plaqueIma = plaqueIma;
    }

    static int getCptVoiture() // pas de const sur une méthode de classe
    {
        return cptVoiture;
    }

    Personne* getProprietaire()
    {
        return proprietaire;
    }

    void setProprietaire(Personne *proprietaire)
    {
        this->proprietaire=proprietaire;
    }

    Moteur& getMoteur()
    {
        return *moteur;
    }

    // Méthodes d'instances
    void accelerer(int vAcc);
    void freiner(int vFrn);
    void arreter();
    bool estArreter() const;
    virtual void afficher() const;

    // Méthode de classe
    static void testMethodeClasse();
    static bool compareVitesse(const Voiture &va, const Voiture &vb);
};
#endif