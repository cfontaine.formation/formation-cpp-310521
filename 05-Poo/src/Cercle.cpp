#include "Cercle.h"
bool Cercle::colision(const Cercle &c1)
{
    return Point::distance(c1.centre, centre) <= (c1.rayon + rayon);
}
bool Cercle::colision(const Cercle &c1, const Cercle &c2)
{
    return Point::distance(c1.centre, c2.centre) <= (c1.rayon + c2.rayon);
}

bool Cercle::contenir(Cercle c1, Point p)
{
    return Point::distance(c1.centre, p) <= c1.rayon;
}

bool Cercle::contenir(Point p)
{
    return Point::distance(centre, p) <= rayon;
}
