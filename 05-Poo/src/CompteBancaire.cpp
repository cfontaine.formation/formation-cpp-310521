#include "CompteBancaire.h"
#include <iostream>

using namespace std;

int CompteBancaire::cptCompte=0;

void CompteBancaire::afficher() const
{
	cout << "Solde = " << solde << endl;
	cout << "Iban = " << iban << endl;
	cout << "Titulaire = " << titulaire << endl;
	cout << "_________________" << endl;
}

void CompteBancaire::crediter(double valeur)
{
	if (valeur > 0) {
		solde += valeur;
	}
}

void CompteBancaire::debiter(double valeur)
{
	if (valeur > 0)
	{
		solde -= valeur;
	}
}

bool CompteBancaire::estPositif() const
{
	return solde > 0;
}
