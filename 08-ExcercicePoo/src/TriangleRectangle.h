#ifndef TRIANGLE_RECTANGLE_H
#define TRIANGLE_RECTANGLE_H
#include "Forme.h"
class TriangleRectangle : public Forme
{
	double largeur;
	double longueur;
public:
	TriangleRectangle(Couleur couleur, double largeur, double longueur) : Forme(couleur), largeur(largeur), longueur(longueur)
	{

	}
	double calculSurface();
};

#endif