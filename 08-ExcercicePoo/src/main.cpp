#include <iostream>
#include "Terrain.h" 
#include "Rectangle.h"
#include "Cercle.h"
#include "TriangleRectangle.h"

using namespace std;

int main()
{
    Terrain t;
    Rectangle rec1(Couleur::BLEU, 1.0, 1.0);
    Rectangle rec2(Couleur::ROUGE, 1.0, 1.0);
    Rectangle rec3(Couleur::ROUGE, 1.0, 1.0);
    Cercle cer1(Couleur::ORANGE,1.0);
    Cercle cer2(Couleur::ORANGE, 1.0);
    TriangleRectangle tr1(Couleur::VERT, 1.0, 1.0);
    t.ajoutForme(rec1);
    t.ajoutForme(rec2);
    t.ajoutForme(rec3);
    t.ajoutForme(tr1);
    t.ajoutForme(cer1);
    t.ajoutForme(cer2);
    cout << "Total=" << t.surfaceTotal() << endl;
    cout << "Rouge= " << t.surfaceTotal(Couleur::ROUGE) << endl;
    cout << "Vert= " << t.surfaceTotal(Couleur::VERT) << endl;
    cout << "Bleu= " << t.surfaceTotal(Couleur::BLEU) << endl;
    cout << "Orange= " <<  t.surfaceTotal(Couleur::ORANGE) << endl;
}
