#ifndef CERCLE_H
#define CERCLE_H

#include "Forme.h"

class Cercle : public Forme
{
    double rayon;

public:
    Cercle(Couleur couleur, double rayon) : Forme(couleur), rayon(rayon)
    {
    }

    double calculSurface();
};

#endif