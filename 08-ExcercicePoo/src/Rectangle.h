#ifndef Rectangle
#define REctangle
#include "Forme.h"

class Rectangle :  public Forme
{
	double largeur;
	double longueur;
public:
	Rectangle(Couleur couleur, double largeur, double longueur) : Forme(couleur), largeur(largeur), longueur(longueur)
	{

	}

	double calculSurface();
};
#endif