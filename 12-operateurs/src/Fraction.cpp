#include "Fraction.h"

double Fraction::valeur()
{
    return static_cast<double>(numerateur)/denominateur;
}

Fraction operator*(const Fraction& f1, const Fraction& f2)
{
    int num = f1.numerateur * f2.numerateur;
    int den = f1.denominateur * f2.denominateur;
    return Fraction(num , den) ;
}

bool operator==(const Fraction& f1, const Fraction& f2)
{
    return f1.numerateur==f2.numerateur && f1.denominateur==f2.denominateur;
}
bool operator!=(const Fraction& f1, const Fraction& f2)
{
    return !(f1 == f2);
}
std::ostream& operator<<(std::ostream& os, const Fraction& f)
{
    return os  << f.numerateur << " / " << f.denominateur ;
}
