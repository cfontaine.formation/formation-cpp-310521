#ifndef POINT_H
#define POINT_H

#include<ostream>

class Point
{
    int x;
    int y;

public:
    Point() : x(0), y(0)
    {
    }

    Point(int x, int y) : x(x), y(y)
    {
    }

    int getX() const
    {
        return x;
    }

    int getY() const
    {
        return y;
    }

    void setX(int x)
    {
        this->x = x;
    }

    void setY(int y)
    {
        this->y = y;
    }

    void afficher() const;
    void deplacer(int tx, int ty);
    double norme() const;

    static double distance(const Point &p1, const Point &p2);

    // on ne peut pas surcharger les opérateurs ? : . .* ::

    Point operator-();
    Point operator++();    // pré-incrémentation
    Point operator++(int); // post-incréméntation
    int &operator[](int);

    friend Point operator+(const Point &p1, const Point &p2);
    friend Point operator*(int m, const Point &p);
    friend bool operator==(const Point &p1, const Point &p2);
    friend bool operator!=(const Point &p1, const Point &p2);
    friend std::ostream& operator<<(std::ostream & os,const Point &p);
};
#endif