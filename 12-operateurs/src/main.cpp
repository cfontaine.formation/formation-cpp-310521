#include <iostream>
#include "Point.h"

using namespace std;

int main()
{
    Point a(3, 2);
    Point b = -a;
    b.afficher();

    Point i = ++a;
    i.afficher();
    a.afficher();

    i = a++;
    i.afficher();
    a.afficher();

    cout << a[0] << endl;
    a[0] = 10;
    a.afficher();
    try
    {
        a[6] = 1;
    }
    catch (out_of_range &e)
    {
        cerr << e.what() << endl;
    }

    Point c = a + b;
    c.afficher();
    c = 2 * a; // a*2 => erreur
    c.afficher();

    cout << (a == b) << endl;
    cout << (a != b) << endl;
    cout << a << endl;
}