#include <ostream>
class Fraction
{
	int numerateur;
	int denominateur;

public:
	Fraction(int numerateur=0, int denominateur=1) : numerateur(numerateur), denominateur(denominateur)
	{

	}
	virtual ~Fraction(){}
	double valeur();
	friend Fraction operator*(const Fraction& f1, const Fraction& f2);
	friend bool operator==(const Fraction& f1, const Fraction& f2);
	friend bool operator!=(const Fraction& f1, const Fraction& f2);
	friend std::ostream& operator<<(std::ostream& os, const Fraction& f);	
};
