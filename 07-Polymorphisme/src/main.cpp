#include "Animal.h"
#include <iostream>
#include "Chien.h"
#include "Chat.h"
#include "Refuge.h"
using namespace std;
int main()
{
  /*  Animal* a1=new Animal(7,5000);
    a1->afficher();
    a1->emmetreSon();*/

    Chien *c1=new Chien(5,4500,"Laika");
    c1->emmetreSon();
    cout<<c1->getNom()<<endl;

    Animal* a2=new Chien(3,7000,"Rolo");
    a2->emmetreSon();

   // Chien*c2=(Chien*)a2;
    Chien*c2=dynamic_cast<Chien*>(a2);
    cout<<c2<<endl;
    if(c2!=0){
        cout<<c2->getNom()<<endl;
    }

    //delete a1;
 
    delete c1;
    delete a2;

    Refuge refuge;
    refuge.ajouter(new Chien(5,4500,"Laika"));
    refuge.ajouter(new Chat(3,5000,7));
    refuge.ajouter(new Chien(5,4500,"Idefix"));
    refuge.ajouter(new Chat(3,4000,9));
    refuge.ajouter(new Chien(5,7000,"Rolo"));
    refuge.ecouter();


}