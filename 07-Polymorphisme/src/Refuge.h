#ifndef REFUGE_H
#define REFUGE_H

#include "Animal.h"

class Refuge{
    Animal* animaux[10];
    int placeOccuper;

    public:
    Refuge() : placeOccuper(0)
    {
        for(int i=0;i<10;i++){
            animaux[i]=0;
        }
    }

    ~Refuge();

    void ajouter(Animal* a);
    void ecouter();
};

#endif