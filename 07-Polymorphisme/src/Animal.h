#ifndef ANIMAL_H
#define ANIMAL_H

class Animal // classe abstraite
{
    int age;
    double poid;

public:
    Animal(int age, double poid) : age(age), poid(poid)
    {
    }

    virtual ~Animal();

    void afficher();

    virtual void emmetreSon()=0; // méthode virtuelle pure
};

#endif