#ifndef CHIEN_H
#define CHIEN_H
#include "Animal.h"
#include <string>

class Chien : public Animal
{
    std::string nom;

public:
    Chien(int age, double poid, const std:: string &nom) : Animal(age, poid), nom(nom)
    {
    }

    std::string getNom() const
    {
        return nom;
    }

    ~Chien();

    void emmetreSon();
};

#endif // !CHIEN_H#define