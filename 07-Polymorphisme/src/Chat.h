#ifndef CHAT_H
#define CHAT_H

#include "Animal.h"

class Chat : public Animal
{
    int nbVie;

    public:
    Chat (int age, double poid, int nbVie=9) : Animal(age,poid) , nbVie(nbVie)
    {

    }

    int getNbVie() const
    {
        return nbVie;
    }

    void emmetreSon();

};

#endif
