#include <iostream>

using namespace std;

int main()
{
    // Condition if
    int v;
    cin >> v;
    if (v > 10)
    {
        cout << "v supérieur à 10" << endl;
    }
    else if (v == 10)
    {
        cout << "v égal à 10" << endl;
    }
    else
    {
        cout << "v inférieur à 10" << endl;
    }

    // Exercice: Moyenne
    // Saisir 2 nombres entiers et afficher la moyenne dans la console
    int a;
    int b;
    cin >> a;
    cin >> b;
    double res = (a + b) / 2.0;
    // ou double moy = static_cast<double>(a + b) / 2;
    cout << "moyenne=" << res << endl;

    // Trie de 2 Valeurs
    // Saisir 2 nombres à virgule flottante et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
    double va;
    double vb;
    cin >> va;
    cin >> vb;
    if (va > vb)
    {
        cout << vb << " < " << va << endl;
    }
    else
    {
        cout << va << " < " << vb << endl;
    }

    // Exercice : Intervalle
    // Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus)
    int vi;
    cin >> vi;
    if (vi > -4 && vi <= 7)
    {
        cout << vi << " fait parti de l'intervalle" << endl;
    }

    // Condition: switch
    int jours;
    cin >> jours;
    switch (jours)
    {
    case 1:
        std::cout << "Lundi" << std::endl;
        break;
    case 6:
    case 7:
        std::cout << "Week end !" << std::endl;
        break;
    default:
        std::cout << "Un autre jour" << std::endl;
        break;
    }

    // Exercice : Calculatrice
    // Faire un programme calculatrice
    //	Saisir dans la console
    //	- un double
    //	- une caractère opérateur qui a pour valeur valide : + - * /
    //	- un double

    // Afficher:
    // - Le résultat de l’opération
    // - Une message d’erreur si l’opérateur est incorrecte
    // - Une message d’erreur si l’on fait une division par 0
    double v1;
    char op;
    double v2;

    cin >> v1;
    cin >> op;
    cin >> v2;
    switch (op)
    {
    case '+':
        cout << v1 << " + " << v2 << " = " << (v1 + v2) << endl;
        break;
    case '-':
        cout << v1 << " - " << v2 << " = " << (v1 - v2) << endl;
        break;
    case '*':
        cout << v1 << " * " << v2 << " = " << (v1 * v2) << endl;
        break;
    case '/':
        if (v2 == 0.0)
        {
            cout << "division par 0" << endl;
        }
        else
        {
            cout << v1 << " / " << v2 << " = " << (v1 / v2) << endl;
        }
        break;
    default:
        cout << op << " n'est pas un operateur valide" << endl;
    }

    // Condition: opérateur ternaire
    // Valeur absolue
    // Saisir un nombre et afficher la valeur absolue sous la forme | -1.85 | = 1.85
    double val;
    cin >> val;
    double res = val > 0 ? val : -val;
    cout << "|" << val << "| = " << res << std::endl;

    // Boucle: while
    int j = 0;
    while (j < 10)
    {
        cout << j << endl;
        j++;
    }

    // Boucle: for
    for (int i = 0; i < 10; i++)
    {
        cout << "i = " << i << endl;
    }

    // Instructions de branchement
    // break
    for (int i = 0; i < 10; i++)
    {
        cout << "i = " << i << endl;
        if (i == 3)
        {
            break; // break => termine la boucle
        }
    }

    // continue
    for (int i = 0; i < 10; i++)
    {
        if (i == 3)
        {
            continue; // continue => on passe à l'itération suivante
        }
        cout << "i = " << i << endl;
    }

    // goto
    for (int i = 0; i < 10; i++)
    {
        for (int k = 0; k < 5; k++)
        {
            cout << "i = " << i << "k= " << k << endl;
            if (i == 3)
            {
                goto EXIT_LOOP; // Utilisation de goto pour sortir de 2 boucles imbriquées
            }
        }
    }
EXIT_LOOP:

    // Exercice : Table de multiplication
    // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
    //	1 X 4 = 4
    //	2 X 4 = 8
    //	…
    //	9 x 4 = 36
    //	Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9, on arrête sinon on redemande une nouvelle valeur
    for (;;) // while(true)  => boucle infinie
    {
        int vm;
        cin >> vm;
        if (vm < 1 || vm > 9)
        {
            break;
        }
        for (int i = 1; i < 9; i++)
        {
            cout << i << " x " << vm << " = " << (i * vm) << endl;
        }
    }

    // Exercice : Quadrillage
    // Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
    //	ex : pour 2 3
    //	[] []
    //	[] []
    //	[] []
    int col, row;
    cout << "Entrer le nombre de colonne";
    cin >> col;
    cout << "Entrer le nombre de ligne";
    cin >> row;
    for (int r = 0; r < row; r++)
    {
        for (int c = 0; c < col; c++)
        {
            cout << "[ ]";
        }
        cout << endl;
    }
    return 0;
}