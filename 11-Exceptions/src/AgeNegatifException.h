#ifndef AGE_NEGATIF_EXCEPTION_H
#define AGE_NEGATIF_EXCEPTION_H
#include <exception>
#include <string>

class AgeNegatifException : public std::exception
{
    std::string message;

public:
    AgeNegatifException(int age) : message("L'age est négatif :" + std::to_string(age)) {}

    const char *what();
};
#endif