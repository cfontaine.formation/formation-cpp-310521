#include <iostream>
#include "AgeNegatifException.h"

using namespace std;

void traitementAge(int age);
void etatCivil(int age);

int main()
{
    int age;
    cin >> age;
    try
    {
        etatCivil(age);
    }
    catch(AgeNegatifException &err)
    {
        cerr<<err.what()<<" Main"<<endl;
    }
    catch(char e)
    {
         cerr<<"age == 0"<<endl;
    }
    catch(...) // tous les autres types d'exception
    {
        cerr<<"age == 1"<<endl;
    }
   
    cout<<"poursuite programme"<<endl;
}

void traitementAge(int age)
{
    cout << "début du traitement" << endl;
    if (age < 0)
    {
        throw AgeNegatifException(age);
    }else if(age==0)
    {
        throw 'n';
    }else if(age==1)
    {
        throw -1.0;
    }
    cout << "fin du traitement" << endl;
}

void etatCivil(int age){
    cout << "début du etatCivil" << endl;
    try
    {
       traitementAge(age);
    }
    catch(AgeNegatifException &e)
    {
        cout<<"Traitement partiel de l'exception : Etat Civil";
        cerr << e.what() << '\n';
      //  throw; // relance de l'exception
      throw 'e';
    }
    
    
    cout << "fin du etatCivil" << endl;
}