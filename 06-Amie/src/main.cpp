#include "Test.h"
#include "ClasseAmie.h"


void fonctionAmie(Test &t){
    t.val=123;
}


int main()
{
    Test t1(1);
    t1.afficher();
    fonctionAmie(t1);
    t1.afficher();

    ClasseAmie ca(t1);
    ca.traitement();
    t1.afficher();
}