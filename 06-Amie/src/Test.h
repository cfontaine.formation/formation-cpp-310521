#ifndef TEST_H
#define TEST_H

//class Other;

class Test
{
    int val;

    public:
    Test(int val) : val(val)
    {

    }

    int getVal() const
    {
        return val;
    } 

    void afficher() const;

    friend void fonctionAmie( Test &t);
    friend class ClasseAmie;
   // friend void Other::methodeAmie(Test &t);
};
#endif